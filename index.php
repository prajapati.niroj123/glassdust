<?php include('header.php') ?>

<div id="home" class="header_slider slider-active">
    <div class="single_slider d-flex align-items-center bg_cover" style="background-image: url(images/slider-1.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-sm-10">
                    <div class="header_content">
                        <h2 class="title" data-animation="fadeInUp" data-delay="0.2s">Crafted for <br>Digital Agency.</h2>
<!--                        <p data-animation="fadeInUp" data-delay="0.5s">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sediam nonumy eirmod tempor invidunt labore.</p>-->
                        <a href="#footer" class="main-btn" data-animation="fadeInUp" data-delay="0.8s">Contact us</a>
                    </div> <!-- header content -->
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </div> <!-- single slider -->

   <!-- <div class="single_slider d-flex align-items-center bg_cover" style="background-image: url(images/slider-2.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-sm-10">
                    <div class="header_content">
                        <h2 class="title" data-animation="fadeInUp" data-delay="0.2s">Creative Digital <br> Agency Template</h2>
                        <p data-animation="fadeInUp" data-delay="0.5s">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sediam nonumy eirmod tempor invidunt labore.</p>
                        <a href="javascript:void(0)" class="main-btn" data-animation="fadeInUp" data-delay="0.8s">Contact us</a>
                    </div>
                </div>
            </div>
        </div>
    </div> -->

   <!-- <div class="single_slider d-flex align-items-center bg_cover" style="background-image: url(images/slider-3.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-sm-10">
                    <div class="header_content">
                        <h2 class="title" data-animation="fadeInUp" data-delay="0.2s">Based on <br> Latest Bootstrap.</h2>
                        <p data-animation="fadeInUp" data-delay="0.5s">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sediam nonumy eirmod tempor invidunt labore.</p>
                        <a href="javascript:void(0)" class="main-btn" data-animation="fadeInUp" data-delay="0.8s">Contact us</a>
                    </div>
                </div>
            </div>
        </div>
    </div>-->

    <!--            <span class="title_shape">DIGITAL</span>-->
</div> <!-- header menu -->


<!--<section id="about" class="about_area pt-80 pb-130">
    <div class="container">
        <div class="row align-items-center">
           <div class="col-lg-6">
                <div class="about_video mt-50 wow fadeInRightBig" data-wow-duration="1.3s" data-wow-delay="0.5s">
                    <img src="images/about-1.jpg" alt="about">
                    <a href="https://www.youtube.com/watch?v=fccI8zccmRs" class="play video-popup"><i class="lni lni-play"></i></a>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="about_content mt-50 wow fadeInLeftBig" data-wow-duration="1.3s" data-wow-delay="0.5s">
                    <div class="section_title">
                        <h4 class="title">About</h4>
                        <h2 class="main_title">Award Winning <span>Digital Agency</span></h2>
                    </div>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore aliquyam erat, sed diam voluptua.</p>

                    <div class="accordion pt-10" id="accordionExample">
                        <div class="card mt-25">
                            <div class="card-header" id="headingOne">
                                <a href="#" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Web Development</a>
                            </div>

                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="card-body">
                                   <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card mt-25">
                            <div class="card-header" id="headingTwo">
                                <a href="#" class="collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">UI/UX Design</a>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                <div class="card-body">
                                  <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card mt-25">
                            <div class="card-header" id="headingThree">
                                <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Business Analysis</a>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                <div class="card-body">
                                   <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>-->

<!--====== ABOUT PART ENDS ======-->

<!--====== COUNTER PART START ======-->

<section id="counter" class="counter_area pt-115 pb-120">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="section_title text-center pb-20">
                    <h4 class="title">Stories</h4>
                    <h2 class="main_title">Achievements in Numbers</h2>
                </div> <!-- section title -->
            </div>
        </div> <!-- row -->

        <div class="row">
            <div class="col-md-3 col-6">
                <div class="single_counter mt-30 wow fadeIn" data-wow-duration="1.3s" data-wow-delay="0.2s">
                    <i class="lni lni-cup"></i>
                    <span class="count counter">2435</span>
                    <p>Project Completed</p>
                </div> <!-- single counter -->
            </div>
            <div class="col-md-3 col-6">
                <div class="single_counter mt-30 wow fadeIn" data-wow-duration="1.3s" data-wow-delay="0.4s">
                    <i class="lni lni-users"></i>
                    <span class="count counter">535</span>
                    <p>Our Happy Clients</p>
                </div> <!-- single counter -->
            </div>
            <div class="col-md-3 col-6">
                <div class="single_counter mt-30 wow fadeIn" data-wow-duration="1.3s" data-wow-delay="0.6s">
                    <i class="lni lni-coffee-cup"></i>
                    <span class="count counter">23</span>
                    <p>Awards Won</p>
                </div> <!-- single counter -->
            </div>
            <div class="col-md-3 col-6">
                <div class="single_counter mt-30 wow fadeIn" data-wow-duration="1.3s" data-wow-delay="0.8s">
                    <i class="lni lni-user"></i>
                    <span class="count counter">41</span>
                    <p>Team Members</p>
                </div> <!-- single counter -->
            </div>
        </div> <!-- row -->
    </div> <!-- container -->
</section>

<!--====== COUNTER PART ENDS ======-->

<!--====== SERVICES PART START ======-->

<section id="services" class="services_area pt-115 pb-120 bg_cover" style="background-image: url(images/services_bg.jpg)">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="section_title text-center pb-20">
                    <h4 class="title">Services</h4>
                    <h2 class="main_title">Our Professional Services</h2>
                </div> <!-- section title -->
            </div>
        </div> <!-- row -->

        <div class="row">
            <div class="col-lg-3 col-sm-6">
                <div class="single_services mt-30 wow fadeInUpBig" data-wow-duration="1.3s" data-wow-delay="0.2s">
                    <i class="lni lni-pallet"></i>
                    <h4 class="services_title">Graphic Design</h4>
                    <!--<p>Donsetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore .</p>-->
                </div> <!-- single services -->
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="single_services mt-30 wow fadeInUpBig" data-wow-duration="1.3s" data-wow-delay="0.4s">
                    <i class="lni lni-code-alt"></i>
                    <h4 class="services_title">Web Design</h4>
<!--                    <p>Donsetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore .</p>-->
                </div> <!-- single services -->
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="single_services mt-30 wow fadeInUpBig" data-wow-duration="1.3s" data-wow-delay="0.6s">
                    <i class="lni lni-target"></i>
                    <h4 class="services_title">Business Analysis</h4>
<!--                    <p>Donsetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore .</p>-->
                </div> <!-- single services -->
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="single_services mt-30 wow fadeInUpBig" data-wow-duration="1.3s" data-wow-delay="0.8s">
                    <i class="lni lni-invention"></i>
                    <h4 class="services_title">Digital Marketing</h4>
                  <!--  <p>Donsetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore .</p>-->
                </div> <!-- single services -->
            </div>
        </div> <!-- row -->
    </div> <!-- container -->
</section>

<!--====== SERVICES PART ENDS ======-->

<!--====== PROJECT PART START ======-->

<section id="project" class="project_area pt-115 pb-120">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="section_title pb-60">
                    <h4 class="title">Portfolio</h4>
                    <h2 class="main_title">Latest Project</h2>
                </div> <!-- section title -->
            </div>
        </div> <!-- row -->
    </div> <!-- container -->
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="single_project">
                    <div class="project_image">
                        <img src="images/graphic-design.jpg" alt="project">
                    </div>
                    <div class="project_content">
                        <h6 class="sub_title">Graphic</h6>
                        <h4 class="title"><a href="#">Design Scratch</a></h4>
                    </div>
                </div> <!-- single project -->
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single_project">
                    <div class="project_image">
                        <img src="images/logo-design.jpg" alt="project">
                    </div>
                    <div class="project_content">
                        <h6 class="sub_title">Branding</h6>
                        <h4 class="title"><a href="#">Logo Design</a></h4>
                    </div>
                </div> <!-- single project -->
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single_project">
                    <div class="project_image">
                        <img src="images/web-design.jpg" alt="project">
                    </div>
                    <div class="project_content">
                        <h6 class="sub_title">Web</h6>
                        <h4 class="title"><a href="#">Web Design</a></h4>
                    </div>
                </div> <!-- single project -->
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single_project">
                    <div class="project_image">
                        <img src="images/web-develop.jpg" alt="project">
                    </div>
                    <div class="project_content">
                        <h6 class="sub_title">Web</h6>
                        <h4 class="title"><a href="#">Web Development</a></h4>
                    </div>
                </div> <!-- single project -->
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single_project">
                    <div class="project_image">
                        <img src="images/UIUX.jpg" alt="project">
                    </div>
                    <div class="project_content">
                        <h6 class="sub_title">UI/UX</h6>
                        <h4 class="title"><a href="#">App Design</a></h4>
                    </div>
                </div> <!-- single project -->
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single_project">
                    <div class="project_image">
                        <img src="images/marketing.jpg" alt="project">
                    </div>
                    <div class="project_content">
                        <h6 class="sub_title">Marketing</h6>
                        <h4 class="title"><a href="#">Banner Design</a></h4>
                    </div>
                </div> <!-- single project -->
            </div>
        </div> <!-- row -->
    </div> <!-- container fluid -->
    <div class="container">
       <!-- <div class="project_btn text-center pt-30">
            <a href="javascript:void(0)" class="main-btn">View All</a>
        </div>--> <!-- row -->
    </div> <!-- container -->
</section>

<!--====== PROJECT PART ENDS ======-->

<!--====== TESTMONIAL PART START ======-->

<!--<section id="testimonial" class="testimonial_area pt-115 pb-120 bg_cover" style="background-image: url(images/testimonial_bg.jpg)">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="section_title section_title_2 text-center pb-50">
                    <h4 class="title">Testimonials</h4>
                    <h2 class="main_title">What Client Says</h2>
                </div>
            </div>
        </div>

        <div class="row testimonial_active">
            <div class="col-lg-6">
                <div class="single_testimonial text-center">
                    <img src="images/author-1.jpg" alt="author">
                    <h5 class="name">Sara A. K.</h5>
                    <span>Graphic Designer</span>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Atvero eos et accusam et justo duo dolores et ea rebum. </p>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="single_testimonial text-center">
                    <img src="images/author-2.jpg" alt="author">
                    <h5 class="name">John Doe</h5>
                    <span>Creative Designer</span>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Atvero eos et accusam et justo duo dolores et ea rebum. </p>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="single_testimonial text-center">
                    <img src="images/author-3.jpg" alt="author">
                    <h5 class="name">Ena Shah</h5>
                    <span>Web Designer</span>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Atvero eos et accusam et justo duo dolores et ea rebum. </p>
                </div>
            </div>
        </div>
    </div>
</section>-->

<!--====== TESTMONIAL PART ENDS ======-->

<!--====== TEAM PART START ======-->

<!--<section id="team" class="team_area pt-115 pb-120">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="section_title text-center pb-20">
                    <h4 class="title">Team</h4>
                    <h2 class="main_title">Meet The Team</h2>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-4 col-sm-7">
                <div class="single_team mt-30 wow fadeIn" data-wow-duration="1.3s" data-wow-delay="0.2s">
                    <div class="team_image">
                        <img src="images/team-1.jpg" alt="team">
                    </div>
                    <div class="team_content">
                        <h4 class="team_name"><a href="#">David Smith</a></h4>
                        <span class="sub_title">UI/UX Designer</span>
                        <a class="arrow" href="javascript:void(0)"><i class="lni lni-chevron-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-7">
                <div class="single_team mt-30 wow fadeIn" data-wow-duration="1.3s" data-wow-delay="0.5s">
                    <div class="team_image">
                        <img src="images/team-2.jpg" alt="team">
                    </div>
                    <div class="team_content">
                        <h4 class="team_name"><a href="#">Sara A. K.</a></h4>
                        <span class="sub_title">Creative Designer</span>
                        <a class="arrow" href="javascript:void(0)"><i class="lni lni-chevron-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-7">
                <div class="single_team mt-30 wow fadeIn" data-wow-duration="1.3s" data-wow-delay="0.8s">
                    <div class="team_image">
                        <img src="images/team-3.jpg" alt="team">
                    </div>
                    <div class="team_content">
                        <h4 class="team_name"><a href="#">Ena Shah</a></h4>
                        <span class="sub_title">Graphic Designer</span>
                        <a class="arrow" href="javascript:void(0)"><i class="lni lni-chevron-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>-->

<!--====== TEAM PART ENDS ======-->

<?php include('footer.php') ?>
<!--====== BLOG PART ENDS ======-->
