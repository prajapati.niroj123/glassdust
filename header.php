<?php if(isset($_POST['submit']) && $_POST['submit']){ 
    $name = $_POST['name'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $subject = $_POST['subject'];
    $message = $_POST['message'];
    
    $error_message = "";
	$email_exp     = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
	
	$email_message = "NEW CONTACT REGISTERED" . "\n";
	$email_message .= "Name: " . $name . "\n";
	$email_message .= "Contact: " . $phone . "\n";
	$email_message .= "Message: " . $message . "\n";
	
	$headers = "From: webmaster@example.com" . "\r\n" ;
	
	$to = "hello@glassdust.digital";
	
	mail($to,$subject,$email_message,$headers);
	
	
    unset($_POST);
} ?>

<!-- Created by Ashish Shrestha -->
<!DOCTYPE html>
<html lang="en">
<head>
    <title></title>
    
    <!------------------------ Meta Starts ---------------------->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width">
    
    <meta property="og:title" content=""/>
    <meta property="og:type"   content="website" />
    <meta property="og:url"    content="" />
    <meta property="og:site_name" content=""/>
    <meta property="og:image" content=""/>
    <meta property="og:image:alt" content="">
    <meta property="og:description" content="">
    <!------------------------- Meta Ends ----------------------->


    <!----------------------- Fonts Starts ---------------------->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!------------------------ Fonts Ends ----------------------->


    <!-------------------- Components Starts -------------------->
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--====== Favicon Icon ======-->
    <link rel="shortcut icon" href="images/favicon.png" type="image/png">

    <!--====== Magnific Popup CSS ======-->
    <link rel="stylesheet" href="css/bowercomponent/fonts/magnific-popup.css">

    <!--====== animate CSS ======-->
    <link rel="stylesheet" href="css/bowercomponent/animate.css">

    <!--====== Slick CSS ======-->
    <link rel="stylesheet" href="css/bowercomponent/slick.css">

    <!--====== Line Icons CSS ======-->
    <link rel="stylesheet" href="css/bowercomponent/fonts/LineIcons.2.0.css">

    <!--====== Bootstrap CSS ======-->
    <link rel="stylesheet" href="css/bowercomponent/bootstrap.4.5.2.min.css">

    <!--====== Default CSS ======-->
    <link rel="stylesheet" href="css/default.css">
    <link rel="stylesheet" href="css/style.css">

    <!--====== Style CSS ======-->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
<!--    <link href="css/bowercomponent/slick-theme.css" rel="stylesheet">-->
    <link href="css/bowercomponent/slick.css" rel="stylesheet">
    <link href="css/bowercomponent/jquery.mCustomScrollbar.min.css" rel="stylesheet">
    <link href="css/bowercomponent/lightbox.min.css" rel="stylesheet">
    <!--------------------- Components Ends --------------------->


    <!-------------------- Site Style Starts -------------------->
    <link href="css/global.css" rel="stylesheet">
    <link href="css/thestyles.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--------------------- Site Style Ends --------------------->

</head>

<body>

<!------------------------------------- Wrapper Starts ------------------------------------->
<div id="wrapper">

    <!--------------------------------- Header Wrapper Starts ---------------------------------->
    <header id="header-wrapper">
        <!--====== PRELOADER PART START ======-->

        <div class="preloader">
            <div class="loader">
                <div class="ytp-spinner">
                    <div class="ytp-spinner-container">
                        <div class="ytp-spinner-rotator">
                            <div class="ytp-spinner-left">
                                <div class="ytp-spinner-circle"></div>
                            </div>
                            <div class="ytp-spinner-right">
                                <div class="ytp-spinner-circle"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--====== PRELOADER PART ENDS ======-->

        <!--====== Header PART START ======-->

        <header class="header_area">
            <div class="header_menu">
                <div class="container-fluid">
                    <div class="header_wrapper d-flex align-items-center justify-content-between">
                        <div class="header_logo">
                            <a href="index.php">
                                <img src="img/logo/gd.png" alt="logo">
                            </a>
                        </div>
                        <div class="header_toggle">
                            <button id="menu_open">
                                <span class="toggle_icon"></span>
                                <span class="toggle_icon"></span>
                                <span class="toggle_icon"></span>
                            </button>
                        </div>
                    </div> <!-- row -->
                </div> <!-- container -->
            </div> <!-- header menu -->
        </header>

        <!--====== Header PART ENDS ======-->

        <!--====== SIDE MENU PART START ======-->

        <div class="side_menu_area">
            <button class="menu_close">
                <span class="close_icon"></span>
                <span class="close_icon"></span>
            </button>
            <div class="side_menu">
                <ul>
                    <li><a class="page-scroll" href="#home">Home</a></li>
                    <li><a class="page-scroll" href="#counter">Stories</a></li>
                    <li><a class="page-scroll" href="#services">Services</a></li>
                    <li><a class="page-scroll" href="#project">Latest  Project</a></li>
                    <!--<li><a class="page-scroll" href="#testimonial">Testimonial</a></li>
                    <li><a class="page-scroll" href="#team">Team</a></li>
                    <li><a class="page-scroll" href="#pricing">Pricing</a></li>
                    <li><a class="page-scroll" href="#blog">Blog</a></li>-->
                    <li><a class="page-scroll" href="#footer">Contact</a></li>
                </ul>
            </div> <!-- side menu -->
        </div> <!-- side menu -->
    </header>
    <!---------------------------------- Header Wrapper Ends ----------------------------------->



    <!-------------------------------- Content Wrapper Starts ---------------------------------->
    <div id="content-wrapper">

