</div>
<!--------------------------------- Content Wrapper Ends ----------------------------------->


<!--------------------------------- Footer Wrapper Starts ---------------------------------->
<footer id="footer-wrapper">
    <section id="footer" class="footer_area">
        <div class="footer_widget bg_cover pt-80 pb-130" style="background-image: url(images/footer_bg.jpg)">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5">
                        <div class="footer_info mt-45">
                            <div class="section_title section_title_2">
                                <h4 class="title">Contact </h4>
                                <h2 class="main_title">Get In Touch</h2>
                            </div> <!-- section title -->

                            <p>Glassdust Digital Media Buy LLC</p>

                            <div class="single_info d-flex align-items-center mt-30">
                                <div class="info_icon">
                                    <I class="lni lni-phone"></I>
                                </div>
                                <div class="info_content media-body">
                                    <p>+1 808-319-8379</p>
                                </div>
                            </div> <!-- single info -->

                            <div class="single_info d-flex align-items-center mt-30">
                                <div class="info_icon">
                                    <I class="lni lni-envelope"></I>
                                </div>
                                <div class="info_content media-body">
                                    <p>hello@glassdust.digital</p>
                                </div>
                            </div> <!-- single info -->

                            <div class="single_info d-flex align-items-center mt-30">
                                <div class="info_icon">
                                    <I class="lni lni-map-marker"></I>
                                </div>
                                <div class="info_content media-body">
                                    <p>77-176 Kekai Place<br> Kailua-Kona <br> Hawaii-96740 <br> United States of America</p>
                                </div>
                            </div> <!-- single info -->
                        </div> <!-- footer info -->
                    </div>
                    <div class="col-lg-7">
                        <div class="footer_form pt-40">
                            <form id="contact-form-hjasddsajkdsajkdsa" action="#" method="post">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="single_input mt-30">
                                            <input type="text" name="name" placeholder="Name">
                                        </div> <!-- single input -->
                                    </div>
                                    <div class="col-md-6">
                                        <div class="single_input mt-30">
                                            <input type="text" name="email" placeholder="Email">
                                        </div> <!-- single input -->
                                    </div>
                                    <div class="col-md-6">
                                        <div class="single_input mt-30">
                                            <input type="text" name="phone" placeholder="Phone">
                                        </div> <!-- single input -->
                                    </div>
                                    <div class="col-md-6">
                                        <div class="single_input mt-30">
                                            <input type="text" name="subject" placeholder="Subject">
                                        </div> <!-- single input -->
                                    </div>
                                    <div class="col-md-12">
                                        <div class="single_input mt-30">
                                            <textarea name="message" placeholder="Message"></textarea>
                                        </div> <!-- single input -->
                                    </div>
                                    <p class="form-message"></p>
                                    <div class="col-md-12">
                                        <div class="single_input mt-30">
                                            <input type="submit" name="submit" value="SUBMIT" class="main-btn">
                                        </div> <!-- single input -->
                                    </div>
                                </div> <!-- row -->
                            </form>
                        </div> <!-- footer form -->
                    </div>
                </div> <!-- row -->
            </div> <!-- container -->
        </div> <!-- footer widget -->

        <div class="footer_copyright">
            <div class="container">
                <div class="footer_copyright_wrapper text-center d-sm-flex justify-content-between align-items-center">
                    <div class="copyright mt-15">
                        <p>Designed and Developed by <a style=" color: #02986A"; href="https://glassdust.digital" rel="nofollow">Glassdust Digital</a></p>
                    </div>
                    <div class="footer_social mt-15">
                        <ul>
                            <li><a href="javascript:void(0)"><i class="lni lni-facebook-filled"></i></a></li>
                            <li><a href="javascript:void(0)"><i class="lni lni-twitter-original"></i></a></li>
                            <li><a href="javascript:void(0)"><i class="lni lni-linkedin-original"></i></a></li>
                            <li><a href="javascript:void(0)"><i class="lni lni-instagram-original"></i></a></li>
                        </ul>
                    </div>
                </div> <!-- footer copyright wrapper -->
            </div> <!-- container -->
        </div> <!-- footer widget -->
    </section>
</footer>
<!---------------------------------- Footer Wrapper Ends ----------------------------------->
</div>
<!-------------------------------------- Wrapper Ends -------------------------------------->

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/slick.min.js"></script>
<script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="js/lightbox.min.js"></script>
<script type="text/javascript" src="js/thescripts.js"></script>
<script src="js/jquery-1.12.4.min.js"></script>
<script src="js/modernizr-3.7.1.min.js"></script>

<!--====== Bootstrap js ======-->
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.4.5.2.min.js"></script>

<!--====== Slick js ======-->
<script src="js/slick.min.js"></script>

<!--====== Ajax Contact js ======-->
<script src="js/ajax-contact.js"></script>

<!--====== Magnific Popup js ======-->
<script src="js/jquery.magnific-popup.min.js"></script>

<!--====== Counter Up js ======-->
<script src="js/waypoints.min.js"></script>
<script src="js/jquery.counterup.min.js"></script>

<!--====== Scrolling Nav js ======-->
<script src="js/jquery.easing.min.js"></script>
<script src="js/scrolling-nav.js"></script>

<!--====== wow js ======-->
<script src="js/wow.min.js"></script>

<!--====== Main js ======-->
<script src="js/main.js?v=1.00"></script>


</body>
</html>